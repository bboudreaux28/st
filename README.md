# ST

Blake Boudreaux's build of [st (the simple terminal)](https://st.suckless.org). This is version 0.8.4 of st. As of writing this, 0.8.4 is the most recent version of st.

More specifically, This is suckless's simple terminal with the following patches
- alpha
- anysize
- boxdraw
- blinking cursor
- scrollback -> scrollback-mouse -> scrollback-mouse-increment

The diff file for each patch is included in the repo so that you know exactly which version of each patch has been applied.

## Hotkeys

Relevant Hotkeys
- alt + c to copy
- alt + v to paste
- alt + j to scroll down
- alt + k to scroll up
- ctrl + shift + k to zoom in
- ctrl + shift + j to zoom out
- alt + f to scroll down a page
- alt + b to scroll up a page

## Fonts

I should mention that this build expects you to have the fonts "Ubuntu Mono" and "Font Awesome" (v4.7.0) installed. You can edit the config.def.h (then copy it to config.h and rebuild st via `sudo make install`) to change the fonts.

## Installation

To install:

In your terminal, navigate to the directory where you would like to put the source files (I use ~/.local/src for this) and type

```
git clone https://gitlab.com/bboudreaux28/st
cd st
sudo make install
```
